# -*- coding: utf-8 -*-
"""Setup/installation tests for this package."""

from aranyani.policy.testing import IntegrationTestCase
from plone import api


class TestInstall(IntegrationTestCase):
    """Test installation of aranyani.policy into Plone."""

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')

    def test_product_installed(self):
        """Test if aranyani.policy is installed with portal_quickinstaller."""
        self.assertTrue(self.installer.isProductInstalled('aranyani.policy'))

    def test_uninstall(self):
        """Test if aranyani.policy is cleanly uninstalled."""
        self.installer.uninstallProducts(['aranyani.policy'])
        self.assertFalse(self.installer.isProductInstalled('aranyani.policy'))

    # browserlayer.xml
    def test_browserlayer(self):
        """Test that IAranyaniPolicyLayer is registered."""
        from aranyani.policy.interfaces import IAranyaniPolicyLayer
        from plone.browserlayer import utils
        self.assertIn(IAranyaniPolicyLayer, utils.registered_layers())
